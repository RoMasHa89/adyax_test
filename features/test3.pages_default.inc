<?php
/**
 * @file
 * test3.pages_default.inc
 */

/**
 * Implements hook_default_page_manager_pages().
 */
function test3_default_page_manager_pages() {
  $page = new stdClass();
  $page->disabled = FALSE; /* Edit this to true to make a default page disabled initially */
  $page->api_version = 1;
  $page->name = 'bla';
  $page->task = 'page';
  $page->admin_title = 'test3';
  $page->admin_description = '';
  $page->path = 'test3';
  $page->access = array();
  $page->menu = array();
  $page->arguments = array();
  $page->conf = array(
    'admin_paths' => FALSE,
  );
  $page->default_handlers = array();
  $handler = new stdClass();
  $handler->disabled = FALSE; /* Edit this to true to make a default handler disabled initially */
  $handler->api_version = 1;
  $handler->name = 'page_bla__panel';
  $handler->task = 'page';
  $handler->subtask = 'bla';
  $handler->handler = 'panel_context';
  $handler->weight = 0;
  $handler->conf = array(
    'title' => 'Panel',
    'no_blocks' => 0,
    'pipeline' => 'standard',
    'body_classes_to_remove' => '',
    'body_classes_to_add' => '',
    'css_id' => '',
    'css' => '',
    'contexts' => array(
      0 => array(
        'identifier' => 'Timestamp Context',
        'keyword' => 'timestamp_context',
        'name' => 'timestamp_context',
        'id' => 1,
      ),
    ),
    'relationships' => array(),
    'name' => 'panel',
  );
  $display = new panels_display();
  $display->layout = 'flexible';
  $display->layout_settings = array();
  $display->panel_settings = array(
    'style_settings' => array(
      'default' => NULL,
      'center' => NULL,
    ),
  );
  $display->cache = array();
  $display->title = 'test3';
  $display->uuid = 'c0ea83ce-b7e5-47d5-90e6-2486c2d40f46';
  $display->content = array();
  $display->panels = array();
    $pane = new stdClass();
    $pane->pid = 'new-87b05793-b7b7-4572-8366-13c323c994d1';
    $pane->panel = 'center';
    $pane->type = 'timestamp_form';
    $pane->subtype = 'timestamp_form';
    $pane->shown = TRUE;
    $pane->access = array(
      'plugins' => array(
        0 => array(
          'name' => 'field_timestamp',
          'settings' => NULL,
          'context' => 'context_timestamp_context_1',
          'not' => FALSE,
        ),
      ),
      'logic' => 'and',
    );
    $pane->configuration = array(
      'context' => 'context_timestamp_context_1',
      'override_title' => 1,
      'override_title_text' => 'Pane with Access:',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 0;
    $pane->locks = array();
    $pane->uuid = '87b05793-b7b7-4572-8366-13c323c994d1';
    $display->content['new-87b05793-b7b7-4572-8366-13c323c994d1'] = $pane;
    $display->panels['center'][0] = 'new-87b05793-b7b7-4572-8366-13c323c994d1';
    $pane = new stdClass();
    $pane->pid = 'new-5abf49e4-7e07-4054-a01d-16c17a59dcf7';
    $pane->panel = 'center';
    $pane->type = 'timestamp_form';
    $pane->subtype = 'timestamp_form';
    $pane->shown = TRUE;
    $pane->access = array(
      'logic' => 'and',
    );
    $pane->configuration = array(
      'context' => 'context_timestamp_context_1',
      'override_title' => 1,
      'override_title_text' => 'Pane without Access:',
      'override_title_heading' => 'h2',
    );
    $pane->cache = array();
    $pane->style = array(
      'settings' => NULL,
    );
    $pane->css = array();
    $pane->extras = array();
    $pane->position = 1;
    $pane->locks = array();
    $pane->uuid = '5abf49e4-7e07-4054-a01d-16c17a59dcf7';
    $display->content['new-5abf49e4-7e07-4054-a01d-16c17a59dcf7'] = $pane;
    $display->panels['center'][1] = 'new-5abf49e4-7e07-4054-a01d-16c17a59dcf7';
  $display->hide_title = PANELS_TITLE_FIXED;
  $display->title_pane = '0';
  $handler->conf['display'] = $display;
  $page->default_handlers[$handler->name] = $handler;
  $pages['bla'] = $page;

  return $pages;

}
