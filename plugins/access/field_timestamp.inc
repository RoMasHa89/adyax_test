<?php
/**
 * @file
 * Plugin to provide access control based upon a parent term.
 */

/**
 * Plugins array.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t('Timestamp-based access'),
  'description' => t('Allow access if the timestamp context returns TRUE'),
  'callback' => 'adyax_test_field_timestamp_ctools_access_check',
  'settings form' => 'adyax_test_field_timestamp_ctools_access_settings',
  'summary' => 'adyax_test_field_timestamp_ctools_access_summary',
  'default' => array('field_timestamp' => 1),
  'required context' => new ctools_context_required(t('Timestamp Context'), 'timestamp_context'),
);


/**
 * Custom callback defined by 'callback' in the $plugin array.
 *
 * Check for access.
 */
function adyax_test_field_timestamp_ctools_access_check($conf, $context) {
  // If for some unknown reason that $context isn't set, return false.
  if (empty($context) || empty($context->data)) {
    return FALSE;
  }

  if ($context->data == TRUE) {
    return TRUE;
  }
  else {
    return FALSE;
  }
}

/**
 * Settings form for the access plugin.
 */
function adyax_test_field_timestamp_ctools_access_settings($form, &$form_state, $conf) {
  return $form;
}


/**
 * Provide a summary description.
 */
function adyax_test_field_timestamp_ctools_access_summary($conf, $context) {
  return t('Checks Timestamp context');
}
