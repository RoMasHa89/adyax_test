<?php
/**
 * @file
 * Contains the content type plugin for a adyax_test panel page.
 */

$plugin = array(
  'title' => t('Timestamp'),
  'description' => t('Displays a timestamp result.'),
  'category' => array(t('Ctools Timestamp')),
  'single' => TRUE,
  'render callback' => 'adyax_test_timestamp_form_content_type_render',
  'edit form' => 'adyax_test_timestamp_form_content_type_edit_form',
  'admin info' => 'adyax_test_timestamp_form_admin_info',
  'defaults' => array(),
  'required context' => new ctools_context_required(t('Timestamp Context'), 'timestamp_context'),
);

/**
 * Title callback for admin page.
 */
function adyax_test_timestamp_form_admin_title($subtype, $conf, $context = NULL) {
  return t('Timestamp pane');
}

/**
 * Callback to provide info (the preview in panels when building a panel).
 */
function adyax_test_timestamp_form_admin_info($subtype, $conf, $context = NULL) {
  $block = new stdClass();
  $block->title = t('Timestamp pane');
  $config = array();

  $config[] = t('Title: @name', array('@name' => $block->title));
  $block->content = theme_item_list(
    array(
      'items' => $config,
      'title' => NULL,
      'type' => 'ul',
      'attributes' => array(),
    )
  );
  
  return $block;
}

/**
 * Edit callback for the content type.
 */
function adyax_test_timestamp_form_content_type_edit_form($form, &$form_state) {
  $conf = $form_state['conf'];

  return $form;
}

/**
 * Submit callback for settings form.
 */
function adyax_test_timestamp_form_content_type_edit_timestamp_form_submit($form, &$form_state) {
  foreach (element_children($form) as $key) {
    if (isset($form_state['values'][$key])) {
      $form_state['conf'][$key] = $form_state['values'][$key];
    }
  }
}


/**
 * Render callback function.
 *
 * @param object $subtype
 *    Subtype content_type.
 * @param array $conf
 *    Configuration array.
 * @param array $args
 *    Arguments.
 * @param object $context
 *    Context content.
 *
 * @return object
 *    Block with context result.
 */
function adyax_test_timestamp_form_content_type_render($subtype, $conf, $args, $context) {
  $data = $context->data;
  $block = new stdClass();
  if ($data) {
    $data = 'TRUE';
  }
  else {
    $data = 'FALSE';
  }
  $block->content = $data;

  return $block;
}
