<?php
/**
 * @file
 * Ctools context type plugin that returns Timestamp result.
 */

/**
 * Plugins array.
 *
 * Plugins are described by creating a $plugin array which will be used
 * by the system that includes this file.
 */
$plugin = array(
  'title' => t("Timestamp Context"),
  'description' => t('A timestamp-based context.'),
  'context' => 'adyax_test_context_create_timestamp_context',
  'context name' => 'timestamp_context',
  'keyword' => 'timestamp_context',

);

/**
 * Create a context.
 *
 * @param bool $empty
 *   If true, just return an empty context.
 * @param array $data
 *   If from settings form, an array as from a form. If from argument, a string.
 * @param bool $conf
 *   TRUE if the $data is coming from admin configuration.
 *
 * @return object
 *   Context object.
 */
function adyax_test_context_create_timestamp_context($empty, $data = NULL, $conf = FALSE) {
  $context = new ctools_context('timestamp_context');
  $context->plugin = 'timestamp_context';

  if ($empty) {
    return $context;
  }

  $time = time();
  if ($time % 2) {
    $result = TRUE;
  }
  else {
    $result = FALSE;
  }
  $context->title = t("Timestamp context");
  $context->data = $result;

  return $context;
}
